# Enoncé TP
- observez le code source de l'application fourni et le jeu de données du fichier data.sql
- exécutez la tâche Gradle ```build > assemble``` : vous ne devriez pas avoir d'erreur
- exécutez la tâche Gradle ```verification > test``` : tous les tests sont en échec
   - sous Intellij vous pouvez lancer les tests avec les boutons ▶️ dans la marge des classes du répertoire test
- complétez la classe CatalogService et si nécessaire ProductRepository et BrandRepository pour que l'exécution des tests se terminent avec succès
- vous n'avez pas le droit de modifier ni les classes Product, ni Brand, ni les classes de test


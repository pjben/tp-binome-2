package org.isima.tp;

interface ICatalogServiceTest {

    Brand APPLE = new Brand("APPLE");
    Product iphoneX64g = new Product("iPhone X 64g", "Mobile");
    Product iphoneX128g = new Product("iPhone XR 128g", "Mobile");
    Product iPhoneXR256g = new Product("iPhone XR 256g", "Mobile");
    Product macBookPro = new Product("Macbook Pro", "PC");
    Product iPad = new Product("iPad", "Tablet");

    Brand LENOVO = new Brand("LENOVO");
    Product chromebook = new Product("Chromebook 300e", "PC");
    Product ideapad530 = new Product("Ideapad 530", "PC");
    Product thinkPadT480 = new Product("ThinkPad T480", "PC");
    Product yoga730 = new Product("Yoga 730", "PC");
    Product x103f = new Product("X103F 10.1", "Tablet");

    Brand HUAWEI = new Brand("HUAWEI");
    Product p8Lite = new Product("P8 Lite", "Mobile");
    Product p20 = new Product("P20", "Mobile");

    Brand SAMSUNG = new Brand("SAMSUNG");
    Product galaxyS8 = new Product("Galaxy S8", "Mobile");
    Product galaxyS9 = new Product("Galaxy S9", "Mobile");
    Product galaxyNote8 = new Product("Galaxy Note 8", "Mobile");
    Product galaxyBook = new Product("Galaxy Book 10.6", "PC");
    Product galaxyTab = new Product("Galaxy Tab S4", "Tablet");

    Brand XIAOMI = new Brand("XIAOMI");
    Product mi8 = new Product("Mi 8", "Mobile");
    Product pocophoneF1 = new Product("Pocophone F1", "Mobile");
    Product miA2 = new Product("Mi A2", "Mobile");
    Product redmiNote6 = new Product("Redmi Note 6", "Mobile");
    Product miPad4 = new Product("Mi Pad 4", "Tablet");
}

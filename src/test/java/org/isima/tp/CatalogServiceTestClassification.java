package org.isima.tp;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Implémenter le traitement qui permet de classer les produits selon leur gamme.
 * Il doit y avoir 1/3 des produits par catégorie de gamme.
 * Il y a 3 catégories :
 * - Entry-level : les produits les moins chers
 * - Top-end : les produits les plus chers
 * - Mid-range : les autres produits
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
class CatalogServiceTestClassification implements ICatalogServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogServiceTestClassification.class);

    @Autowired
    private
    CatalogService catalogService;

    @Test
    void testMobilesByRange() {
        Map<String, List<Product>> mobilesByRange = catalogService.getProductsByRange("Mobile");
        assertNotNull(mobilesByRange);
        assertEquals(3, mobilesByRange.keySet().size());

        // entrée de gamme = le tiers des produits les moins chers
        List<Product> entryLevelMobiles = mobilesByRange.get("Entry-level");
        LOGGER.info("\n\n---------\nENTRY LEVEL MOBILES\n---------");
        LOGGER.info(entryLevelMobiles.toString());
        assertNotNull(entryLevelMobiles);
        assertEquals(4, entryLevelMobiles.size());
        assertTrue(entryLevelMobiles.containsAll(Arrays.asList(miA2, redmiNote6, p20, p8Lite)));

        // haut de gamme = le tiers des produits les plus chers
        List<Product> topEndMobiles = mobilesByRange.get("Top-end");
        LOGGER.info("\n\n---------\nTOP END MOBILES\n---------");
        LOGGER.info(topEndMobiles.toString());
        assertNotNull(topEndMobiles);
        assertEquals(4, topEndMobiles.size());
        assertTrue(topEndMobiles.containsAll(Arrays.asList(iPhoneXR256g, iphoneX128g, galaxyS9, iphoneX64g)));

        // milieu de gamme = les autres
        List<Product> midRangeMobiles = mobilesByRange.get("Mid-range");
        LOGGER.info("\n\n---------\nMID RANGE MOBILES\n---------");
        LOGGER.info(midRangeMobiles.toString());
        assertNotNull(midRangeMobiles);
        assertEquals(4, midRangeMobiles.size());
        assertTrue(midRangeMobiles.containsAll(Arrays.asList(pocophoneF1, mi8, galaxyS8, galaxyNote8)));
    }


    @Test
    void testPcByRange() {
        Map<String, List<Product>> pcByRange = catalogService.getProductsByRange("PC");
        assertNotNull(pcByRange);
        assertEquals(3, pcByRange.keySet().size());

        // entrée de gamme = le tiers des produits les moins chers
        List<Product> entryLevelPc = pcByRange.get("Entry-level");
        LOGGER.info("\n\n---------\nENTRY LEVEL PC\n---------");
        LOGGER.info(entryLevelPc.toString());
        assertNotNull(entryLevelPc);
        assertEquals(2, entryLevelPc.size());
        assertTrue(entryLevelPc.containsAll(Arrays.asList(chromebook, galaxyBook)));

        // haut de gamme = le tiers des produits les plus chers
        List<Product> topEndPc = pcByRange.get("Top-end");
        LOGGER.info("\n\n---------\nTOP END MOBILES\n---------");
        LOGGER.info(topEndPc.toString());
        assertNotNull(topEndPc);
        assertEquals(2, topEndPc.size());
        assertTrue(topEndPc.containsAll(Arrays.asList(thinkPadT480, macBookPro)));

        // milieu de gamme = les autres
        List<Product> midRangePc = pcByRange.get("Mid-range");
        LOGGER.info("\n\n---------\nMID RANGE PC\n---------");
        LOGGER.info(midRangePc.toString());
        assertNotNull(midRangePc);
        assertEquals(2, midRangePc.size());
        assertTrue(midRangePc.containsAll(Arrays.asList(yoga730, ideapad530)));
    }

    @Test
    void testUnknownType() {
        Map<String, List<Product>> byRange = catalogService.getProductsByRange("xxx");
        assertNotNull(byRange);
        assertEquals(3, byRange.keySet().size());

        // entrée de gamme
        List<Product> entryLevel = byRange.get("Entry-level");
        assertNotNull(entryLevel);
        assertTrue(entryLevel.isEmpty());

        // haut de gamme
        List<Product> topEnd = byRange.get("Top-end");
        assertNotNull(topEnd);
        assertTrue(topEnd.isEmpty());

        // milieu de gamme
        List<Product> midRange = byRange.get("Mid-range");
        assertNotNull(midRange);
        assertTrue(midRange.isEmpty());
    }
}

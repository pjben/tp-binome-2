package org.isima.tp;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Implémenter le traitement qui permet de calculer
 * le prix moyen d'un article d'un type donné,
 * avec la précision donnée
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
class CatalogServiceTestAverage implements ICatalogServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogServiceTestAverage.class);
    @Autowired
    private
    CatalogService catalogService;

    @Test
    void testAverageTabletPrice() {
        BigDecimal averageTabletPrice = catalogService.averagePrice("Tablet", 1);

        assertNotNull(averageTabletPrice);

        LOGGER.info("\n\n---------\nAverage tablet price\n---------");
        LOGGER.info(averageTabletPrice.toString());

        assertEquals(429.0D, averageTabletPrice.doubleValue());
    }

    @Test
    void testAverageMobilePrice() {
        BigDecimal averageMobilePrice = catalogService.averagePrice("Mobile", 10);

        assertNotNull(averageMobilePrice);

        LOGGER.info("\n\n---------\nAverage mobile price\n---------");
        LOGGER.info(averageMobilePrice.toString());

        assertEquals(514.99D, averageMobilePrice.doubleValue());
    }

    @Test
    void testUnknownTypePrice() {
        BigDecimal averageUnknownTypePrice = catalogService.averagePrice("unknown type;", 5);

        assertNotNull(averageUnknownTypePrice);

        LOGGER.info("\n\n---------\nAverage unknown price\n---------");
        LOGGER.info(averageUnknownTypePrice.toString());

        assertEquals(0D, averageUnknownTypePrice.doubleValue());
    }

}

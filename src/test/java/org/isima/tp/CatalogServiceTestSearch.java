package org.isima.tp;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Implémenter une recherche de produit qui permettra de rechercher les produits
 * - par modèle contenant le texte passé en paramètre et en ignorant la casse
 * - par marque contenant le texte passé en paramètre et en ignorant la casse
 * - par prix si le texte passé en paramètre est de la forme "prixmin/prixmax"
 *   -> prixmin et prixmax sont alors 2 nombres entiers définissant l'intervalle de prix (inclusive)
 *      prixmin <= prix <= prixmax
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
class CatalogServiceTestSearch implements ICatalogServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogServiceTestSearch.class);

    @Autowired
    private
    CatalogService catalogService;

    @Autowired
    private
    ProductRepository productRepository;

    @Test
    void testSearchByModelExact() {

        String searchTerm = iphoneX64g.getName();

        Set<Product> products = catalogService.searchProducts(searchTerm);
        assertNotNull(products);

        LOGGER.info("\n\n---------\nSearch result for '{}' \n---------", searchTerm);
        LOGGER.info(products.toString());

        assertEquals(1, products.size());
        assertTrue(products.contains(iphoneX64g));
    }

    @Test
    void testSearchByModelContains() {

        String searchTerm = "iPhone";

        Set<Product> products = catalogService.searchProducts(searchTerm);
        assertNotNull(products);

        LOGGER.info("\n\n---------\nSearch result for '{}' \n---------", searchTerm);
        LOGGER.info(products.toString());

        assertEquals(3, products.size());
        assertTrue(products.containsAll(Arrays.asList(iphoneX64g, iphoneX128g, iPhoneXR256g)));
    }

    @Test
    void testSearchByBrand() {

        String searchTerm = APPLE.getName();

        Set<Product> products = catalogService.searchProducts(searchTerm);
        assertNotNull(products);

        LOGGER.info("\n\n---------\nSearch result for '{}' \n---------", searchTerm);
        LOGGER.info(products.toString());

        assertEquals(5, products.size());
        assertTrue(products.containsAll(Arrays.asList(iphoneX64g, iphoneX128g, iPhoneXR256g, macBookPro, iPad)));
    }

    @Test
    void testSearchByBrandLowerCase() {

        String searchTerm = APPLE.getName().toLowerCase();

        Set<Product> products = catalogService.searchProducts(searchTerm);
        assertNotNull(products);

        LOGGER.info("\n\n---------\nSearch result for '{}' \n---------", searchTerm);
        LOGGER.info(products.toString());

        assertEquals(5, products.size());
        assertTrue(products.containsAll(Arrays.asList(iphoneX64g, iphoneX128g, iPhoneXR256g, macBookPro, iPad)));
    }

    @Test
    void testSearchNoResult() {

        String searchTerm = "no match!";

        Set<Product> products = catalogService.searchProducts(searchTerm);
        assertNotNull(products);

        LOGGER.info("\n\n---------\nSearch result for '{}' \n---------", searchTerm);
        LOGGER.info(products.toString());

        assertEquals(0, products.size());

    }

    @Test
    void testSearchProductWithoutBrand() {

        // Ajout d'un produit sans marque
        Product onePlus6T = new Product();
        onePlus6T.setId(99L);
        onePlus6T.setName("OnePlus 6T");
        onePlus6T.setPrice(475D);
        onePlus6T.setType("Mobile");
        productRepository.save(onePlus6T);

        String searchTerm = "6T";

        Set<Product> products = catalogService.searchProducts(searchTerm);
        assertNotNull(products);

        LOGGER.info("\n\n---------\nSearch result for '{}' \n---------", searchTerm);
        LOGGER.info(products.toString());

        assertEquals(1, products.size());
        assertEquals(onePlus6T, products.iterator().next());

        productRepository.delete(onePlus6T);
    }

    @Test
    void testSearchNull() {

        Set<Product> products = catalogService.searchProducts(null);
        assertNotNull(products);

        LOGGER.info("\n\n---------\nSearch result for null \n---------");
        LOGGER.info(products.toString());

        assertEquals(0, products.size());

    }

    @Test
    void testSearchByPrice() {

        String searchTerm = "545/700";

        Set<Product> products = catalogService.searchProducts(searchTerm);
        assertNotNull(products);

        LOGGER.info("\n\n---------\nSearch result for '{}' \n---------", searchTerm);
        LOGGER.info(products.toString());

        assertEquals(4, products.size());
        assertTrue(products.containsAll(Arrays.asList(galaxyTab, galaxyNote8, iphoneX64g, galaxyBook)));
    }

    @Test
    void testSearchNotByPrice() {

        String searchTerm = "289€/299€"; // l'intervalle contient des caractères non numériques (€) donc on ne doit PAS rechercher par prix

        Set<Product> products = catalogService.searchProducts(searchTerm);
        assertNotNull(products);

        LOGGER.info("\n\n---------\nSearch result for '{}' \n---------", searchTerm);
        LOGGER.info(products.toString());

        assertEquals(0, products.size());
    }
}

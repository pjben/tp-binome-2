package org.isima.tp;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Implémenter 2 traitements :
 * - listProductsByBrands qui permet de classer les produits par marque.
 * - listBrandsByProductType qui permet de classer les marques par type de produit
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
class CatalogServiceTestMaps implements ICatalogServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogServiceTestMaps.class);
    @Autowired
    private
    CatalogService catalogService;

    @Test
    void testListProductsByBrands() {
        Map<Brand, List<Product>> map = catalogService.listProductsByBrands();
        assertNotNull(map);

        LOGGER.info("\n\n---------\nCATALOG\n---------");
        map.forEach((brand, products) ->
                LOGGER.info("{} : {}", brand.getName(), products));

        assertEquals(map.size(), 5);
        assertTrue(map.containsKey(APPLE));
        assertTrue(map.get(APPLE).containsAll(Arrays.asList(iphoneX64g, iphoneX128g, iPhoneXR256g, macBookPro, iPad)));
        assertTrue(map.containsKey(LENOVO));
        assertTrue(map.get(LENOVO).containsAll(Arrays.asList(chromebook, ideapad530, thinkPadT480, yoga730, x103f)));
        assertTrue(map.containsKey(HUAWEI));
        assertTrue(map.get(HUAWEI).containsAll(Arrays.asList(p8Lite, p20)));
        assertTrue(map.containsKey(SAMSUNG));
        assertTrue(map.get(SAMSUNG).containsAll(Arrays.asList(galaxyBook, galaxyNote8, galaxyS8, galaxyS9, galaxyTab)));
        assertTrue(map.containsKey(XIAOMI));
        assertTrue(map.get(XIAOMI).containsAll(Arrays.asList(mi8, pocophoneF1, miA2, redmiNote6, miPad4)));
    }

    @Test
    void testListBrandsByProductType() {
        Map<String, Set<Brand>> map = catalogService.listBrandsByProductType();
        assertNotNull(map);

        LOGGER.info("\n\n---------\nBRANDS\n---------");
        map.forEach((productType, brands) -> LOGGER.info("{} : {}", productType, brands.toString()));

        assertEquals(map.size(), 3);
        assertTrue(map.containsKey("PC"));
        assertTrue(map.get("PC").containsAll(Arrays.asList(LENOVO, APPLE, SAMSUNG)));
        assertTrue(map.containsKey("Tablet"));
        assertTrue(map.get("Tablet").containsAll(Arrays.asList(LENOVO, XIAOMI, APPLE, SAMSUNG)));
        assertTrue(map.containsKey("Mobile"));
        assertTrue(map.get("Mobile").containsAll(Arrays.asList(APPLE, SAMSUNG, XIAOMI, HUAWEI)));
    }
}

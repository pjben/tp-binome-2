package org.isima.tp;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.security.InvalidParameterException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Implémenter 2 traitements :
 * - createNewProduct qui permet de créer un nouveau produit.
 * - findProductById qui permet de retrouver un produit depuis son identifiant
 * Si le produit ne correspond pas aux règles de validations décrites dans les tests,
 * une exception de type InvalidParameterException doit être lancée.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
class CatalogServiceTestCreate implements ICatalogServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogServiceTestCreate.class);
    @Autowired
    private
    CatalogService catalogService;

    @Test
    void testCreateOK() {
        Product newProduct = new Product();
        newProduct.setType("Mobile");
        newProduct.setName("Mi Mix 2S");
        newProduct.setPrice(369.99D);
        newProduct.setBrandId(3L);

        newProduct = catalogService.createNewProduct(newProduct);
        assertNotNull(newProduct);
        assertNotNull(newProduct.getId());
        assertTrue(catalogService.findProductById(newProduct.getId()).isPresent());
    }

    @Test
    void testCreateUnknownBrandId() {
        Product newProduct = new Product();
        newProduct.setType("Mobile");
        newProduct.setName("Mi Mix 2S");
        newProduct.setPrice(369.99D);
        newProduct.setBrandId(9999L);

        Exception e = assertThrows(InvalidParameterException.class,
                () -> catalogService.createNewProduct(newProduct));
        assertEquals("Brand with id 9999 does not exist", e.getMessage());
    }

    @Test
    void testCreateExistingProductForBrand() {
        Product newProduct = new Product();
        newProduct.setType("Mobile");
        newProduct.setName(redmiNote6.getName());
        newProduct.setPrice(369.99D);
        newProduct.setBrandId(3L);

        Exception e = assertThrows(InvalidParameterException.class,
                () -> catalogService.createNewProduct(newProduct));
        assertEquals("Product with this name already exists for Brand with (id=\"3\")", e.getMessage());
    }
}

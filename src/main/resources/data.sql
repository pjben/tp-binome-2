insert into brand values (1, 'APPLE');
insert into brand values (2, 'SAMSUNG');
insert into brand values (3, 'XIAOMI');
insert into brand values (4, 'LENOVO');
insert into brand values (5, 'HUAWEI');

insert into product (brand_id, type, price, name, id) values(1, 'Mobile', 599.99, 'iPhone X 64g', 1);
insert into product (brand_id, type, price, name, id) values(1, 'Mobile', 899.99, 'iPhone XR 128g', 2);
insert into product (brand_id, type, price, name, id) values(1, 'Mobile', 1299.99, 'iPhone XR 256g', 3);
insert into product (brand_id, type, price, name, id) values(1, 'PC', 2199.99, 'Macbook Pro', 4);
insert into product (brand_id, type, price, name, id) values(1, 'Tablet', 799.99, 'iPad', 5);

insert into product (brand_id, type, price, name, id) values(2, 'Mobile', 499.99, 'Galaxy S8', 6);
insert into product (brand_id, type, price, name, id) values(2, 'Mobile', 799.99, 'Galaxy S9', 7);
insert into product (brand_id, type, price, name, id) values(2, 'Mobile', 589.99, 'Galaxy Note 8', 8);
insert into product (brand_id, type, price, name, id) values(2, 'PC', 699.99, 'Galaxy Book 10.6', 9);
insert into product (brand_id, type, price, name, id) values(2, 'Tablet', 545.00, 'Galaxy Tab S4', 10);

insert into product (brand_id, type, price, name, id) values(3, 'Mobile', 399.99, 'Mi 8', 11);
insert into product (brand_id, type, price, name, id) values(3, 'Mobile', 299.99, 'Pocophone F1', 12);
insert into product (brand_id, type, price, name, id) values(3, 'Mobile', 199.99, 'Mi A2', 13);
insert into product (brand_id, type, price, name, id) values(3, 'Mobile', 199.99, 'Redmi Note 6', 14);
insert into product (brand_id, type, price, name, id) values(3, 'Tablet', 199.99, 'Mi Pad 4', 15);

insert into product (brand_id, type, price, name, id) values(4, 'PC', 399.99, 'Chromebook 300e', 16);
insert into product (brand_id, type, price, name, id) values(4, 'PC', 899.99, 'Ideapad 530', 17);
insert into product (brand_id, type, price, name, id) values(4, 'PC', 1099.99, 'ThinkPad T480', 18);
insert into product (brand_id, type, price, name, id) values(4, 'PC', 899.99, 'Yoga 730', 19);
insert into product (brand_id, type, price, name, id) values(4, 'Tablet', 199.99, 'X103F 10.1', 20);

insert into product (brand_id, type, price, name, id) values(5, 'Mobile', 99.99, 'P8 Lite', 21);
insert into product (brand_id, type, price, name, id) values(5, 'Mobile', 289.99, 'P20', 22);

insert into product (brand_id, type, price, name, id) values(null, 'Tablet', 399.99, 'Mediapad M5', 23);

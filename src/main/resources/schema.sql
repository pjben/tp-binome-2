CREATE TABLE brand (
	id number(10) primary key,
	name varchar(128)
);

CREATE TABLE product (
	id number(10) primary key,
	type varchar(128),
	price number(8,2),
	name varchar(2000),
	brand_id number(10)
);

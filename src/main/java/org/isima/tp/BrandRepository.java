package org.isima.tp;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface BrandRepository extends JpaRepository<Brand, Long> {

    // Test search
    Set<Brand> findByNameLikeIgnoreCase(String searchTerm);
}

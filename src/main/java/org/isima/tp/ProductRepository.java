package org.isima.tp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface ProductRepository extends JpaRepository<Product, Long> {

    // Test Maps
    List<Product> findByBrandId(Long id);

    // Test Average
    Integer countByType(String type);
    @Query("select sum(price) from Product where type = ?1")
    Double sumByType(String type);

    // Test Search
    Set<Product> findByNameLikeIgnoreCase(String productName);
    Set<Product> findByPriceBetweenOrPriceIsOrPriceIs(Double min, Double max, Double min1, Double max1);

    // Test create
    @Query("select max(id) from Product")
    Long findMaxId();

}

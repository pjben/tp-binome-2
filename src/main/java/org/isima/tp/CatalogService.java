package org.isima.tp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.InvalidParameterException;
import java.util.*;
import java.util.stream.Collectors;

@Service
class CatalogService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogService.class);

    @Autowired
    private
    BrandRepository brandRepository;

    @Autowired
    private
    ProductRepository productRepository;

    // Test map
    Map<Brand, List<Product>> listProductsByBrands() {
        Map<Brand, List<Product>> result = new HashMap<>();

        brandRepository.findAll().forEach(brand -> {
            List<Product> products = productRepository.findByBrandId(brand.getId());
            result.put(brand, products);
        });

        return result;
    }

    Map<String, Set<Brand>> listBrandsByProductType() {
        Map<String, Set<Brand>> result = new HashMap<>();

        productRepository.findAll().forEach(product -> {
            if (product.getBrandId() != null) {
                brandRepository.findById(product.getBrandId()).ifPresent(brand -> {
                    result.putIfAbsent(product.getType(), new HashSet<>());
                    result.get(product.getType()).add(brand);
                });
            }
        });

        return result;
    }

    // Test average
    public BigDecimal averagePrice(String type, Integer precision) {
        Integer countByType = productRepository.countByType(type);
        BigDecimal countByTypeDecimal = countByType == null ? BigDecimal.ZERO : BigDecimal.valueOf(countByType);
        Double sumByType = productRepository.sumByType(type);
        BigDecimal sumByTypeDecimal = sumByType == null ? BigDecimal.ZERO : BigDecimal.valueOf(sumByType);
        return countByType == null ||countByType.equals(0) ? BigDecimal.ZERO : sumByTypeDecimal.divide(countByTypeDecimal, precision, RoundingMode.HALF_UP);
    }

    // Test search
    public Set<Product> searchProducts(String searchTerm) {
        Set<Product> products = new HashSet<>();

        if (searchTerm != null) {
            String[] strings = searchTerm.split("/");
            Double min = null;
            Double max = null;
            if (strings.length == 2) {
                try {
                    min = Double.parseDouble(strings[0]);
                    max = Double.parseDouble(strings[1]);
                } catch (NumberFormatException e) {
                    LOGGER.error(null, e);
                }
            }

            if (min != null && max != null) {
                products = productRepository.findByPriceBetweenOrPriceIsOrPriceIs(min, max, min, max);
            } else {
                products = productRepository.findByNameLikeIgnoreCase("%" + searchTerm + "%");
                Set<Brand> brands = brandRepository.findByNameLikeIgnoreCase("%" + searchTerm + "%");
                for (Brand brand : brands) {
                    products.addAll(productRepository.findByBrandId(brand.getId()));
                }
            }
        }

        return products;
    }

    // Test classification
    public Map<String, List<Product>> getProductsByRange(String type) {

        Map<String, List<Product>> result = new HashMap<>();

        List<Product> products = productRepository.findAll().stream()
            .filter(p -> p.getType().equals(type))
            .sorted(Comparator.comparing(Product::getPrice))
            .collect(Collectors.toList());

        int nbOfProductByRange = products.size() / 3;

        result.put("Entry-level", products.stream().limit(nbOfProductByRange).collect(Collectors.toList()));

        products.sort(Comparator.comparing(Product::getPrice).reversed());
        result.put("Top-end", products.stream().limit(nbOfProductByRange).collect(Collectors.toList()));

        products.removeAll(result.get("Entry-level"));
        products.removeAll(result.get("Top-end"));
        result.put("Mid-range", products);

        return result;
    }

    // Test create
    public Product createNewProduct(Product product) {

        product.setId(productRepository.findMaxId()+1);

        Brand brand = brandRepository.findById(product.getBrandId())
            .orElseThrow(() -> new InvalidParameterException(String.format("Brand with id %d does not exist", product.getBrandId())));

        Optional<Product> existing = productRepository.findByBrandId(product.getBrandId()).stream()
                .filter(p -> p.getName().equalsIgnoreCase(product.getName())).findFirst();
        if (existing.isPresent()) {
            throw new InvalidParameterException(String.format("Product with this name already exists for Brand with (id=\"%s\")", brand.getId()));
        }

        return productRepository.save(product);
    }

    public Optional<Product> findProductById(Long id) {
        return productRepository.findById(id);
    }
}
